This repo exists in order to verify 
[my account on framagit.org](https://framagit.org/frnmst/)
for my Keyoxide identity proofs.

You can check out my identity proofs on Keyoxide for the following address:
- [frnmst \D\o\T keyoxide {-A-T-} outlook \D\o\T com](https://keyoxide.org/396da54dc8019e4f4522cbd5a3fa3c2b4230215a)
  (public key [`396da54dc8019e4f4522cbd5a3fa3c2b4230215a`](https://blog.franco.net.eu.org/pubkeys/pgp_pubkey_frnmst_keyoxide.txt))
